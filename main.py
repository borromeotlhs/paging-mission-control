import json
import datetime

class Watcher:
  def __init__(self):
    self.tstat = []
    self.batt = []
    self.treadings = []
    self.breadings = []
  
  # ensures we stay in a 5 minute window
  # also appends the reading if it meets alarm criteria, ignores otherwise
  def checkTime(self, now, sat, component, raw_val, t_high, b_low,output):
    print "{} {}".format(len(self.tstat),len(self.batt))
    
    d,t = now.split(' ')
    time = datetime.datetime.strptime(t, '%H:%M:%S.%f')
    
    if component == 'TSTAT\n':
      if raw_val > t_high:
        self.tstat.append(time)
        self.treadings.append(
          {
            'satelliteId' : sat,
            'severity' : 'RED HIGH',
            'component' : component[:-1],
            'timestamp' : '{}T{}Z'.format(d,t)
          }
        )
        #prune if outside 5 minute window
        if len(self.tstat) > 0:
          if time - self.tstat[0] > datetime.timedelta(minutes=5): 
            self.tstat = self.tstat[1:] # move all measurements down one
            self.treadings = self.treadings[1:]
      
        if len(self.tstat) == 3: # >3 is the same condition
            output.append(self.treadings[0])
      

    elif component == 'BATT\n':
      if raw_val < b_low:
        self.batt.append(time)
        self.breadings.append(
          {
            'satelliteId' : sat_id,
            'severity' : 'RED LOW',
            'component' : component[:-1],
            'timestamp' : '{}T{}Z'.format(d,t)
          }
        )
      
        if len(self.batt) > 0:
          if time - self.batt[0] > datetime.timedelta(minutes=5):
            self.batt = self.batt[1:]
            self.breadings = self.breadings[1:]
        
        if len(self.batt) == 3:
          output.append(self.breadings[0])

      
    




if __name__ == '__main__':
  sat_1 = Watcher()
  sat_2 = Watcher()
  # use json library to print out a list of dicts
  output = []
  f = open('input3.dat','r')
  for line in f:
    time,sat_id,r_hi,_,_,r_low,raw_val,component = line.split('|')
    d,t = time.split(' ')
    
    #satellite 1
    if sat_id == '1000':
      sat_1.checkTime(time, sat_id, component, raw_val,r_hi,r_low,output)
      
    
    # satellite 2
    elif sat_id == '1001':
      sat_2.checkTime(time, sat_id, component, raw_val,r_hi,r_low)

  
  f.close()

  print json.dumps(output) # print out the list of dictionaries as json